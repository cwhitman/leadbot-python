from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, BigInteger, String, DateTime, Float, Boolean
from sqlalchemy.orm import sessionmaker

from bs4 import BeautifulSoup

from urllib.parse import urlparse

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait as wait

from datetime import datetime
from operator import itemgetter
from random import seed
from random import random
from random import randint

import json
import time
import dateutil.parser
import os
import boto3
import requests
import slack
import ssl as ssl_lib
import certifi
import timeit
import logging
import settings
import config

# DATABASE

DB_URI = 'mysql+pymysql://leadbot_admin:emit8582@localhost/leadbot'
engine = create_engine(DB_URI, echo=False)

Base = declarative_base()

class Listing(Base):
    __tablename__ = 'listings'
    id = Column(Integer, primary_key=True)
    posted = Column(DateTime)
    date = Column(String(12))
    title = Column(String(150))
    link = Column(String(150), unique=True)
    state_country = Column(String(25))
    location = Column(String(15))
    category = Column(String(3))
    search_term = Column(String(25))
    cl_id = Column(BigInteger, unique=True)
    show_flag = Column(Boolean, unique=False, default=True)

class Blacklist(Base):
    __tablename__ = 'blacklist'
    id = Column(Integer, primary_key=True)
    title = Column(String(256))
    cl_id = Column(BigInteger, unique=True)

Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()

# GET POSTINGS FROM QUERY STRING

def get_postings_from_params(state, loc, cat, query_str):

    LISTING_URL = ["https://{}.craigslist.org/search/{}?query={}".format(loc, cat, query_str)]

    logging.debug(' Scraping..')
    logging.debug('\t* FROM: {} - {}'.format(state, loc))
    logging.debug('\t* QUERY: {}\n'.format(query_str))

    results=[]

    for url in LISTING_URL:

        try:
            r = requests.get(url, allow_redirects=False)
            # print(r)
        except:
            # print('Invalid URL')
            config.ERRCT += 1
            err_url = ' INVALID URL\n\t| URL: {}\n\t| STATE: {}\n\t| LOCATION: {}\n'.format(url, state, loc)
            config.ERRLIST.append(err_url)
            logging.debug('Connection to {} is invalid. Moving to next item.'.format(url))
            break

        if r.status_code == 200:
            pass
            # print('Resp: 200')
        elif (r.status_code == 301 or r.status_code == 302):

            config.REDIRCT += 1
            orig_url_netloc = urlparse(r.url).netloc
            orig_netloc = orig_url_netloc.split('.')[0]

            r = requests.get(url)
            url = r.url
            parsed_redirect = urlparse(url)
            parsed_netloc = parsed_redirect.netloc
            loc = parsed_netloc.split('.')[0]

            redir_msg = ' REDIRECTED URL\n\t| FROM: {}\n\t| TO {}\n\t| STATE: {}\n\t| LOCATION: {}\n'.format(orig_url_netloc, parsed_netloc, state, orig_netloc)
            config.REDIRLIST.append(redir_msg)
            # print('----REDIRECT BREAK----')
        
        response = r.text
        soup = BeautifulSoup(response, "html.parser")
        links = soup.find_all('p', {'class':'result-info'})

        for link in links:
            title = link.find('a', {'class':'result-title'})
            post_url = title['href']            
            date = link.find('time', {'class':'result-date'})

            search_loc = urlparse(url).netloc
            # print(search_loc)
            post_loc = urlparse(post_url).netloc
            # print(post_loc)

            # Skip out of region postings
            if( post_loc != search_loc ):
                continue
            
            # print(title.text)
            
            result = {}
            result['id'] = title['data-id']

            result['posted'] = date['datetime']
            dt_conv = datetime.strptime(result['posted'], '%Y-%m-%d %H:%M')
            result['date'] = dt_conv.strftime('%b %d, %Y')

            result['title'] = title.text.title()

            result['state'] = state.capitalize()
            result['location'] = loc
            result['category'] = cat
            result['search_term'] = query_str
            # Get domain from URL
            result['url'] = title['href']

            # get_post_email(result['url'])

            results.append(result)

    results.reverse()
    return results

def get_post_email(url):
    options = webdriver.ChromeOptions()
    # options.add_argument('headless')

    driver = webdriver.Chrome(chrome_options=options)
    driver.get(url)
    driver.implicitly_wait(10)
    driver.find_element_by_class_name('reply-button').click()
    driver.implicitly_wait(10)
    post_reply_email_element = driver.find_element_by_class_name('anonemail')
    print(post_reply_email_element)
    post_reply_email = post_reply_email_element.text
    print(post_reply_email)
    driver.implicitly_wait(10)

def scrape():
    """
    Scrapes CL for results,
    returns list of new results.
    """

    LOCS = settings.LISTING_LOCS
    CATS = settings.LISTING_CATS
    SQS = settings.LISTING_SQS

    new_results = []

    for state in LOCS:
        for loc in LOCS[state]:
            for cat in CATS:
                for sq in SQS:

                    possible_results = get_postings_from_params(state, loc, cat, sq)
                    newres_ct = 0
                    blklst_ct = 0
                    for result in possible_results:

                        # For debugging prints params from all results
                        # print(result["title"] + " - " + result["url"])

                        blacklist_titles = []
                        for title in session.query(Blacklist.title).distinct():
                            blacklist_titles.append(title[0])

                        blacklisted = any(ele in result["title"] for ele in blacklist_titles)
                        
                        if blacklisted:
                            config.BLKLISTCT += 1
                            blklst_ct += 1

                            blklist_msg = ' BLACKLISTED ITEM\n\t| TITLE: {}\n\t| ID No. {}\n\t| STATE: {}\n\t| LOCATION: {}\n'.format(result["title"], result["id"], state, loc)
                            config.BLKLISTITEMS.append(blklist_msg)

                            does_entry_exist = session.query(Blacklist).filter_by(cl_id=result["id"]).first()

                            if does_entry_exist is None:
                                blacklistentry = Blacklist(
                                    title=result["title"],
                                    cl_id=result["id"]
                                )

                                session.add(blacklistentry)
                                session.commit()

                        elif not blacklisted:

                            listing = session.query(Listing).filter_by(cl_id=result["id"]).first()

                            if listing is None:
                                new_results.append(result)
                                newres_ct += 1
                    logging.debug('')
                    logging.debug(" Results for '{}' in {}, {} * ".format(sq.upper(), state.upper(), loc.upper()))
                    logging.debug("\t* {} New Results".format(newres_ct))
                    logging.debug("\t* {} Blacklisted Results\n\n--------------------------------\n".format(blklst_ct))
    
    sorted_results = sorted(new_results, key=itemgetter('posted'))

    # for result in sorted_results:
    #     print("{} - {}".format(result["posted"], result["location"]))

    removal_idx_ctr = 0
    removal_index = []

    logging.debug('Writing to Database...\n')
    for result in sorted_results:

        listing = session.query(Listing).filter_by(cl_id=result["id"]).first()
        
        if listing is None:
            listing = Listing(
                posted=result["posted"],
                date=result["date"],
                title=result["title"],
                link=result["url"],
                state_country=result["state"],
                location=result["location"],
                category=result["category"],
                search_term=result["search_term"],
                cl_id=result["id"]
            )

            session.add(listing)
            session.commit()

            # logging.debug(' * New entry for {} from {}, {}.'.format(result["search_term"], result["state"], result["location"]))
        
        else:
            removal_index.append(removal_idx_ctr)
            # logging.debug(' * Duplicate entry for {} from {}, {} found'.format(result["search_term"], result["state"], result["location"]))

            # print("Index: {} - ID: {}".format(i, result["id"]))

        # Increment removal index counter
        removal_idx_ctr += 1

    logging.debug('Duplicates Found: {}\n'.format(len(removal_index)))

    removal_index.reverse()
    for index in removal_index:
        # logging.debug('\t * Duplicate of postID {} from {}, {} removed.'.format(sorted_results[index]["id"], sorted_results[index]["state"], sorted_results[index]["location"]))
        sorted_results.pop(index)
    
    if removal_idx_ctr > 0:
        logging.debug(' * List DeDupe Complete * \n')
        
    return sorted_results

def exec_scrape():
    """
    runs the scraper
    """
    now = datetime.now().strftime("%Y-%d-%m %H:%M:%S")
    logging.info('\t-- LEADBOT --\t{}\n--------------------------------------\n'.format(now))

    start = timeit.default_timer()

    # slack_token = config.SLACK_TOKEN
    # client = slack.WebClient(token=slack_token)

    all_results = scrape()
    
    # sleepCount = 0
    # for result in all_results:
    #     post_listing_to_slack(client, result)
    #     # time.sleep(2*random())
    #     sleepCount += 1
    #     if sleepCount >= 10:
    #         time.sleep(5)
    #         sleepCount = 0

    stop = timeit.default_timer()

    result_ct = len(all_results)
    exectime = round(stop - start, 2)
    errors = config.ERRCT
    blacklisted = config.BLKLISTCT
    redirects = config.REDIRCT
    now = datetime.now().strftime("%Y-%d-%m %H:%M:%S")

    logging.info('-----------------ERRORS-----------------\n')
    if len(config.ERRLIST) == 0:
        logging.info(' No Errors\n')
    for error in config.ERRLIST:
        logging.error(error)
    logging.info('---------------BLACKLIST----------------\n')
    if len(config.BLKLISTITEMS) == 0:
        logging.info(' No Blacklisted Results\n')
    for item in config.BLKLISTITEMS:
        logging.info(item)
    logging.info('---------------REDIRECTS----------------\n')
    if len(config.REDIRLIST) == 0:
        logging.info(' No Redirects\n')
    for redirect in config.REDIRLIST:
        logging.info(redirect)

    logging.info('   --------------STATS---------------\n')
    logging.info('\tFOUND:\t\t{} New Listings'.format(result_ct))
    logging.info('\tEXEC. TIME:\t{} Seconds'.format(exectime))
    logging.info('\tERRORS:\t\t{} Errors'.format(errors))
    logging.info('\tBLACKLIST:\t{} Items'.format(blacklisted))
    logging.info('\tREDIRECTS:\t{} Redirects\n\n--------------------------------------------'.format(redirects))
    
    logging.info(' Scrape Complete\t{}\n--------------------------------------------\n'.format(now))

    # fname = 'leadbot.log'

    # with open(fname, 'r') as fin:
    #     print(fin.read())

def post_listing_to_slack(sc, listing):
    """
    Posts the listing to slack.
    :param sc: A slack client.
    :param listing: A record of the listing.
    """
    desc = "<{}|*{}*> | {}".format( listing["url"], listing["title"], listing["date"])
    state = "*STATE:* {}".format(listing["state"])
    loc = "*LOCATION:* {}".format(listing["location"])
    sq =  "*SEARCH TERM:* {}".format(listing["search_term"])
    sc.chat_postMessage(
        channel=config.SLACK_CHANNEL,
        blocks=[
            {
                "type": "section",
                "text": {
                    "text": desc,
                    "type": "mrkdwn"
                }
            },
            {
                "type": "context",
                "elements": [
                    {
                        "type": "mrkdwn",
                        "text": state
                    },
                    {
                        "type": "mrkdwn",
                        "text": loc
                    },
                    {
                        "type": "mrkdwn",
                        "text": sq
                    }
                ]
            },
            {
                "type": "divider"
            }
        ]
    )

exec_scrape()