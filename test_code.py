import requests
from urllib.parse import urlparse

def locationDictTest():
    LL_TEST = {
        'Nevada': ['elko', 'reno', 'lasvegas'],
        'Alaska': ['anchorage', 'fairbanks', 'kenai', 'juneau']
    }

    for state in LL_TEST:
        print(state.upper())
        for loc in LL_TEST[state]:
            print('* {}'.format(loc.capitalize()))

        print('-----')

TEST_URL = [
    'https://vt.craigslist.org/search/ggg?query=website',
    'https://nh.craigslist.org'
]

def is_url_valid():
    try:
        r = requests.get(TEST_URL)
    except Exception:
        print("URL NOT VALID")
        # break
    # else:
    #     print('URL valid')

# locationDictTest()

# is_url_valid()

def test_redir_resp():
    for url in TEST_URL:
        r = requests.get(url, allow_redirects=False)
        print (r)
        print(r.status_code)
        if (r.status_code == 301 or r.status_code == 302):
            print('redirected')
            r = requests.get(url)
            print(r)
            redirected_url = r.url
            print(redirected_url)

            parsed_redirect = urlparse(redirected_url)
            parsed_netloc = parsed_redirect.netloc

            print(parsed_netloc.split('.')[0])
            
            print('----REDIRECT BREAK----')
            continue
        elif r.status_code == 200:
            print('direct connection, valid')

    


test_redir_resp()