# Production Lists
LISTING_LOCS = {
    'Alabama': [
        'auburn',
        'bham',
        'dothan',
        'shoals',
        'gadsden',
        'huntsville',
        'mobile',
        'montgomery',
        'tuscaloosa'
    ],
    'Alaska': [
        'anchorage',
        'fairbanks',
        'kenai',
        'juneau'
    ],
    'Arizona': [
        'flagstaff',
        'mohave',
        'phoenix',
        'prescott',
        'showlow',
        'sierravista',
        'tucson',
        'yuma'
    ],
    'Arkansas': [
        'fayar',
        'fortsmith',
        'jonesboro',
        'littlerock',
        'texarkana'
    ],
    'California': [
        'bakersfield',
        'chico',
        'fresno',
        'goldcountry',
        'hanford',
        'humboldt',
        'imperial',
        'inlandempire',
        'losangeles',
        'mendocino',
        'merced',
        'modesto',
        'monterey',
        'orangecounty',
        'palmsprings',
        'redding',
        'sacramento',
        'sandiego',
        'slo',
        'santabarbara',
        'santamaria',
        'sfbay',
        'siskiyou',
        'stockton',
        'susanville',
        'ventura',
        'visalia',
        'yubasutter'
    ],
    'Colorado': [
        'boulder',
        'cosprings',
        'denver',
        'eastco',
        'fortcollins',
        'rockies',
        'pueblo',
        'westslope'
    ],
    'Connecticut': [
        'newlondon',
        'hartford',
        'newhaven',
        'nwct'
    ],
    'Delaware': [
        'delaware'
    ],
    'District of Columbia': [
        'washingtondc'
    ],
    'Florida': [
        'daytona',
        'keys',
        'fortmyers',
        'gainesville',
        'cfl',
        'jacksonville',
        'lakeland',
        'lakecity',
        'ocala',
        'okaloosa',
        'orlando',
        'panamacity',
        'pensacola',
        'sarasota',
        'miami',
        'spacecoast',
        'staugustine',
        'tallahassee',
        'tampa',
        'treasure'
    ],
    'Georgia': [
        'albanyga',
        'athensga',
        'atlanta',
        'augusta',
        'brunswick',
        'columbusga',
        'macon',
        'nwga',
        'savannah',
        'statesboro',
        'valdosta'
    ],
    'Hawaii': [
        'honolulu'
    ],
    'Idaho': [
        'boise',
        'eastidaho',
        'lewiston',
        'twinfalls'
    ],
    'Illinois': [
        'bn',
        'chambana',
        'chicago',
        'decatur',
        'lasalle',
        'mattoon',
        'peoria',
        'rockford',
        'carbondale',
        'springfieldil',
        'quincy'
    ],
    'Indiana': [
        'bloomington',
        'evansville',
        'fortwayne',
        'indianapolis',
        'kokomo',
        'tippecanoe',
        'muncie',
        'richmondin',
        'southbend',
        'terrehaute'
    ],
    'Iowa': [
        'ames',
        'cedarrapids',
        'desmoines',
        'dubuque',
        'fortdodge',
        'iowacity',
        'masoncity',
        'quadcities',
        'siouxcity',
        'ottumwa',
        'waterloo'
    ],
    'Kansas': [
        'lawrence',
        'ksu',
        'nwks',
        'salina',
        'seks',
        'swks',
        'topeka',
        'wichita'
    ],
    'Kentucky': [
        'bgky',
        'eastky',
        'lexington',
        'louisville',
        'owensboro',
        'westky'
    ],
    'Louisiana': [
        'batonrouge',
        'cenla',
        'houma',
        'lafayette',
        'lakecharles',
        'monroemi',
        'neworleans',
        'shreveport'
    ],
    'Maine': [
        'maine'
    ],
    'Maryland': [
        'annapolis',
        'baltimore',
        'easternshore',
        'frederick',
        'smd',
        'westmd'
    ],
    'Massachusetts': [
        'boston',
        'capecod',
        'southcoast',
        'westernmass',
        'worcester'
    ],
    'Michigan': [
        'annarbor',
        'battlecreek',
        'centralmich',
        'detroit',
        'flint',
        'grandrapids',
        'holland',
        'jxn',
        'kalamazoo',
        'lansing',
        'monroe',
        'muskegon',
        'nmi',
        'porthuron',
        'saginaw',
        'swmi',
        'thumb',
        'up'
    ],
    'Minnesota': [
        'bemidji',
        'brainerd',
        'duluth',
        'mankato',
        'minneapolis',
        'rmn',
        'marshall',
        'stcloud'
    ],
    'Mississippi': [
        'gulfport',
        'hattiesburg',
        'jackson',
        'meridian',
        'northmiss',
        'natchez'
    ],
    'Missouri': [
        'columbiamo',
        'joplin',
        'kansascity',
        'kirksville',
        'loz',
        'semo',
        'springfield',
        'stjoseph',
        'stlouis'
    ],
    'Montana': [
        'billings',
        'bozeman',
        'butte',
        'greatfalls',
        'helena',
        'kalispell',
        'missoula',
        'montana'
    ],
    'Nebraska': [
        'grandisland',
        'lincoln',
        'northplatte',
        'omaha',
        'scottsbluff'
    ],
    'Nevada': [
        'elko',
        'reno',
        'lasvegas'
    ],
    'New Hampshire': [
        'nh'
    ],
    'New Jersey': [
        'cnj',
        'jerseyshore',
        'newjersey',
        'southjersey'
    ],
    'New Mexico': [
        'albuquerque',
        'clovis',
        'farmington',
        'lascruces',
        'roswell',
        'santafe'
    ],
    'New York': [
        'albany',
        'binghamton',
        'buffalo',
        'catskills',
        'chautauqua',
        'elmira',
        'fingerlakes',
        'glensfalls',
        'hudsonvalley',
        'ithaca',
        'longisland',
        'newyork',
        'oneonta',
        'plattsburgh',
        'potsdam',
        'rochester',
        'syracuse',
        'twintiers',
        'utica',
        'watertown'
    ],
    'North Carolina': [
        'asheville',
        'boone',
        'charlotte',
        'eastnc',
        'fayetteville',
        'greensboro',
        'hickory',
        'onslow',
        'outerbanks',
        'raleigh',
        'wilmington',
        'winstonsalem'
    ],
    'North Dakota': [
        'bismarck',
        'fargo',
        'grandforks',
        'nd'
    ],
    'Ohio': [
        'akroncanton',
        'ashtabula',
        'athensohio',
        'chillicothe',
        'cincinnati',
        'cleveland',
        'columbus',
        'dayton',
        'limaohio',
        'mansfield',
        'sandusky',
        'toledo',
        'tuscarawas',
        'youngstown',
        'zanesville'
    ],
    'Oklahoma': [
        'lawton',
        'enid',
        'oklahomacity',
        'stillwater',
        'tulsa'
    ],
    'Oregon': [
        'bend',
        'corvallis',
        'eastoregon',
        'eugene',
        'klamath',
        'medford',
        'oregoncoast',
        'portland',
        'roseburg',
        'salem'
    ],
    'Pennsylvania': [
        'altoona',
        'chambersburg',
        'erie',
        'harrisburg',
        'lancaster',
        'allentown',
        'meadville',
        'philadelphia',
        'pittsburgh',
        'poconos',
        'reading',
        'scranton',
        'pennstate',
        'williamsport',
        'york'
    ],
    'Rhode Island': [
        'providence'
    ],
    'South Carolina': [
        'charleston',
        'columbia',
        'florencesc',
        'greenville',
        'hiltonhead',
        'myrtlebeach'
    ],
    'South Dakota': [
        'nesd',
        'csd',
        'rapidcity',
        'siouxfalls',
        'sd'
    ],
    'Tennessee': [
        'chattanooga',
        'clarksville',
        'cookeville',
        'jacksontn',
        'knoxville',
        'memphis',
        'nashville',
        'tricities'
    ],
    'Territories': [
        'micronesia',
        'puertorico',
        'virgin'
    ],
    'Texas': [
        'abilene',
        'amarillo',
        'austin',
        'beaumont',
        'brownsville',
        'collegestation',
        'corpuschristi',
        'dallas',
        'nacogdoches',
        'delrio',
        'elpaso',
        'galveston',
        'houston',
        'killeen',
        'laredo',
        'lubbock',
        'mcallen',
        'odessa',
        'sanangelo',
        'sanantonio',
        'sanmarcos',
        'bigbend',
        'texoma',
        'easttexas',
        'victoriatx',
        'waco',
        'wichitafalls'
    ],
    'Utah': [
        'logan',
        'ogden',
        'provo',
        'saltlakecity',
        'stgeorge'
    ],
    'Vermont': [
        'vermont'
    ],
    'Virginia': [
        'charlottesville',
        'danville',
        'fredericksburg',
        'norfolk',
        'harrisonburg',
        'lynchburg',
        'blacksburg',
        'richmond',
        'roanoke',
        'swva',
        'winchester'
    ],
    'Washington': [
        'bellingham',
        'kpr',
        'moseslake',
        'olympic',
        'pullman',
        'seattle',
        'skagit',
        'spokane',
        'wenatchee',
        'yakima'
    ],
    'West Virginia': [
        'charlestonwv',
        'martinsburg',
        'huntington',
        'morgantown',
        'wheeling',
        'parkersburg',
        'swv',
        'wv'
    ],
    'Wisconsin': [
        'appleton',
        'eauclaire',
        'greenbay',
        'janesville',
        'racine',
        'lacrosse',
        'madison',
        'milwaukee',
        'northernwi',
        'sheboygan',
        'wausau'
    ],
    'Wyoming': [
        'wyoming'
    ],
    'Australia': [
        'adelaide',
        'brisbane',
        'cairns',
        'canberra',
        'darwin',
        'goldcoast',
        'melbourne',
        'ntl',
        'perth',
        'sydney',
        'hobart',
        'wollongong'
    ],
    'New Zealand': [
        'auckland',
        'christchurch',
        'dunedin',
        'wellington'
    ],
    'Mexico': [
        'acapulco',
        'bajasur',
        'chihuahua',
        'juarez',
        'guadalajara',
        'guanajuato',
        'hermosillo',
        'mazatlan',
        'mexicocity',
        'monterrey',
        'oaxaca',
        'puebla',
        'pv',
        'tijuana',
        'veracruz',
        'yucatan'
    ],
    'Nicaragua': [
        'managua'
    ],
    'Panama': [
        'panama'
    ],
    'Peru': [
        'lima'
    ],
    'Uruguay': [
        'montevideo'
    ],
    'Venezuela': [
        'caracas'
    ],
    'Caribean Islands': [
        'caribbean'
    ],
    'United Kingdom': [
        'aberdeen',
        'bath',
        'belfast',
        'birmingham',
        'brighton',
        'bristol',
        'cambridge',
        'cardiff',
        'coventry',
        'derby',
        'devon',
        'dundee',
        'norwich',
        'eastmids',
        'edinburgh',
        'essex',
        'glasgow',
        'hampshire',
        'kent',
        'leeds',
        'liverpool',
        'london',
        'manchester',
        'newcastle',
        'nottingham',
        'oxford',
        'sheffield'
    ],
    'Alberta': [
        'calgary',
        'edmonton',
        'ftmcmurray',
        'lethbridge',
        'hat',
        'peace',
        'reddeer'
    ],
    'British Columbia': [
        'cariboo',
        'comoxvalley',
        'abbotsford',
        'kamloops',
        'kelowna',
        'nanaimo',
        'princegeorge',
        'skeena',
        'sunshine',
        'vancouver',
        'victoria',
        'whistler'
    ],
    'Manitoba': [
        'winnipeg'
    ],
    'New Brunswick': [
        'newbrunswick'
    ],
    'Newfoundland and Labrador': [
        'newfoundland'
    ],
    'Northwest Territories': [
        'territories',
        'yellowknife'
    ],
    'Nova Scotia': [
        'halifax'
    ],
    'Ontario': [
        'barrie',
        'belleville',
        'brantford',
        'chatham',
        'cornwall',
        'guelph',
        'hamilton',
        'kingston',
        'kitchener',
        'londonon',
        'niagara',
        'ottawa',
        'owensound',
        'peterborough',
        'sarnia',
        'soo',
        'sudbury',
        'thunderbay',
        'toronto',
        'windsor'
    ],
    'Prince Edward Island': [
        'pei'
    ],
    'Quebec': [
        'montreal',
        'quebec',
        'saguenay',
        'sherbrooke',
        'troisrivieres'
    ],
    'Saskatchewan': [
        'regina',
        'saskatoon'
    ],
    'Yukon Territory': [
        'whitehorse'
    ],
    'South Africa': [
        'capetown',
        'durban',
        'johannesburg',
        'pretoria'
    ],
}

LISTING_CATS = [
    'ggg'
]

LISTING_SQS = [
    'website', #r4b
    'web site', 
    'web design', #REVIEW - GOOD!
    'web designer', # r4b
    'website design', # TBD?
    'web development', 
    'website development', #tbd r4b
    'web developer',
    'website developer', #tbd
    'website needed',
    'build a website', #TbD
    'site',
    'design',
    'designer', 
    'graphic design', #TBD
    'logo', # R4BL
    'logo design', #TBD
    'logo needed', # TBD
    'graphic designer', # TBD? R4B
    'logo designer', # TBD
    'database',
    'server', #TBD Review for BLIST
    'AWS',
    'SQL',
    'java',
    'javascript',
    'html', 
    'css', #TBD
    'js', 
    'php', #TBD? -Rev for BLIST
    'seo',
    'search engine', #TBD
    'wordpress',
    'squarespace', #TBD
    'wix', # TBD
    'weebly', # TBD - Rev for BList
    'shopify', #TBD?
    'godaddy', #tbd
    'go daddy', #tbd
    'word press', # tbd
    'square space', #TBD
    'brand',
    'strategy', 
    'strategist', #TBD
    'social media',
    'facebook',
    'instagram', 
    'linkedin', #TBD - review for blacklists
    'twitter', #TBD?
    'e-commerce',
    'ecommerce', #TBD?
    'development', 
    'django', #TBD
    'drupal', #TBD
    'slack',
    'cloud',
    'GCP', #tbd
    'amazon web', #TBD
    'mobile app',
    'marketing',
    'strategy',
    'email marketing',
    'podcast',
    'podcasting', #TBD
    'web',
    'design',
    'graphic'
]

# Lists for Testing

LISTING_LOCS_T = {
    'New Hampshire': ['nh'],
    'Vermont': ['vermont'],
    'Florida': ['fortlauderdale', 'westpalmbeach'],
    'Alaska': ['anchorage', 'fairbanks', 'kenai', 'juneau']
}

LISTING_SQS_T = [
    'website'
]